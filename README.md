# GIS Tools LidarView Plugin

The primary objective of this plugin is to integrate the features of our [GIS Libraries](https://gitlab.kitware.com/keu-computervision/gis-libraries) into LidarView (and ParaView), facilitating the visualization of results. Additionally, the plugin offers the capability to export point cloud data in `.geopkg` format.

## License

This code is distributed under the Apache 2.0 license.
